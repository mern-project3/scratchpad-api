const mongoose = require("mongoose");

const scratchPadSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
    unique: [true, "Duplicate"],
  },
  content: {
    type: String,
    required: true,
  },
  timestamp: {
    type: String,
    default: Date.now(),
  },
});

module.exports = mongoose.model("ScratchPad", scratchPadSchema);
