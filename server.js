const express = require("express");
const mongoose = require("mongoose");

const ScratchPad = require("./models/ScratchPad");

const app = express();

// MongoDB Atlas Connection
mongoose.connect(
  "mongodb+srv://henry-255:admin123@zuitt-bootcamp.wydh8oh.mongodb.net/scratchpad?retryWrites=true&w=majority&maxIdleTimeMS=30000",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);
mongoose.connection.once("open", () =>
  console.log("Now connected to MongoDB Atlas")
);

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Return all scratch pads
app.get("/", async (req, res) => {
  try {
    const scratchPads = await ScratchPad.find({});
    console.log(scratchPads);
    res.status(200).json(scratchPads);
  } catch (error) {
    res.status(500).json({ message: "Internal Server Error" });
  }
});

// Return scratch pad by Id
app.get("/:id", async (req, res) => {
  try {
    const id = req.params.id;
    const scratchPad = await ScratchPad.findById(id);
    console.log(scratchPad);
    res.status(200).json(scratchPad);
  } catch (error) {
    res.status(500).json({ message: "Internal Server Error" });
  }
});

// adds new scratch pad
app.post("/", async (req, res) => {
  try {
    const { title, content } = req.body;
    const scratchPad = await ScratchPad.create({
      title,
      content,
    });
    console.log(scratchPad);
    res.status(200).json(scratchPad);
  } catch (error) {
    res.status(409).json({ message: "Duplicate" });
  }
});

// update an existing scratch pad
app.put("/:id", async (req, res) => {
  try {
    const id = req.params.id;
    const { title, content } = req.body;
    const scratchPad = await ScratchPad.findByIdAndUpdate(id, {
      title,
      content,
    });
    res.status(200).json(scratchPad);
  } catch (error) {
    res.status(500).json({ message: "Internal Server Error" });
  }
});

// delete scratch pad by Id
app.delete("/:id", async (req, res) => {
  try {
    const id = req.params.id;
    const scratchPad = await ScratchPad.findByIdAndRemove(id);
    console.log(scratchPad);
    res.status(200).json(scratchPad);
  } catch (error) {
    res.status(500).json({ message: "Internal Server Error" });
  }
});

app.listen(3001, () => {
  console.log("listen on port 3001");
});
